'use strict';

angular.module('myApp.students', [])


    .controller('studentsCtrl', ['$rootScope', '$scope', 'students', function ($rootScope, $scope, students) {
        document.getElementById('name').focus();

        $scope.order = '-grade';
        $scope.students = students;

        students.addStudent('John', 93);
        students.addStudent('Beth', 86);
        students.addStudent('Jack', 78);
        students.addStudent('Sue', 63);
        students.addStudent('Paul', 58);

        $scope.addStudent = function(form) {

            form.submitted = true;
            document.getElementById('name').focus();
            if (form.$valid) {
                students.addStudent($scope.name, $scope.grade);
                form.submitted = false;
                $scope.name = undefined;
                $scope.grade = undefined;
                return true;
            } else {
                return false;
            }

        };

        $scope.remove = function(student){
            return students.remove(student);
        }

    }])

/**
 * Students service
 * Add, remove students;  Work out stats and pass rate
 */
    .service('students', ['student', function (student) {

    this.students = [];
    this.passRate = 65;

    this.failed = function (grade) {
        return  this.passRate > grade;
    };

    this.addStudent = function (name, grade) {
        this.students.push(new student(name, grade));
        this.getStats();
    };

    this.remove = function (student) {
        var index = this.students.indexOf(student);
        this.students.splice(index,1);
        this.getStats();
    };

    this.getStats = function () {

        this.stats = {
            min: undefined,
            max: undefined,
            avg: undefined,
            sum: undefined
        };

        var _this = this;
        angular.forEach(this.students, function (student) {
            if (_this.stats.min == undefined) {
                _this.stats.min = student.grade
            } else {
                if (student.grade < _this.stats.min) _this.stats.min = student.grade
            }
            if (_this.stats.max == undefined) {
                _this.stats.max = student.grade
            } else {
                if (student.grade > _this.stats.max) _this.stats.max = student.grade
            }
            if (_this.stats.sum == undefined) {
                _this.stats.sum = student.grade
            } else {
                _this.stats.sum += student.grade
            }
        });

        this.stats.avg = (this.stats.sum / this.students.length);
        return this.stats;
    }
}])

/**
 * Student factory
 * Creates a new student object
 * @property string name Name of Student
 * @property number grade Grade of student
 */
    .factory('student', function () {

        var item = function (name, grade) {
            this.name = name;
            this.grade = parseFloat(grade);
        };

        return item;

    });