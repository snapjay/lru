'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['myApp.students'])

.run(function(){

})
.directive( 'editInPlace',['$timeout', function($timeout) {
  return {
    restrict: 'E',
    scope: {
      value: '=',
      label: '@',
      type: '@'
    },
    templateUrl: 'template/editstudent.html',

    link: function ($scope, element ) {
          $scope.editing = false;

      $scope.edit = function () {
        $scope.editing = true;
        $scope.editValue =  $scope.value;
        $timeout(function() {
          element.find('input')[0].focus();
        }, 50, false);
      };


    },

    controller: ['$rootScope', '$scope', function($rootScope, $scope) {


      $scope.keyUp = function(keyEvent) {

        if(keyEvent.keyCode =='13') { //Enter
          $scope.save();
        }
        if(keyEvent.keyCode =='27') { //Escape
          $scope.cancel();
        }
      };

      $scope.save = function() {
        $scope.ngForm.submitted = true;
        if ($scope.ngForm.$valid){
          $scope.editing = false;
          $scope.value = ($scope.editValue);
        }
      };

      $scope.cancel = function() {
        $scope.editValue =  $scope.value;
        $scope.editing = false;
      }
    }]
  };
}]);